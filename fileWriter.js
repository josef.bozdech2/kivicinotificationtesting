const fs = require('fs')

const writeResultsToFile = (dirname, filename, data) => {
    createDirIfNotExists(dirname);
    fs.appendFileSync(`./${dirname}/${filename}`, `${JSON.stringify(data)},`, err => {})
}

const createDirIfNotExists = (dirname) => {
    const _path = `./${dirname}`;
    if(!fs.existsSync(_path)){
        fs.mkdirSync(_path);
    }
}

module.exports = {writeResultsToFile}