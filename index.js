const prompt = require("prompt-sync")();
const { printScenarioOptions, printScenarioDialogue, createPromptDialogue } = require("./dialogueMaker");
const { writeResultsToFile } = require("./fileWriter");
const { runScenario } = require("./scenarioRunner");
const {enablePing, addComment, disablePing} = require("./testsMaker");
const scenarioTypeCount = 4;
process.env.NODE_TLS_REJECT_UNAUTHORIZED = 0;

async function main(){
    //reset iptables
    enablePing();
    //ask username
    let nickname = prompt("Your nickname: ");
    //ask scenario type
    let scenarioType;
    printScenarioOptions();
    while(isNaN(scenarioType)){
        scenarioType = prompt("Scenario you want to test: ");
        
        // scenarioType = parseInt(scenarioType);
    }
    //print instructions
    printScenarioDialogue(scenarioType);
    // run scenario
    let testResult = {};
    try {
        testResult = await runScenario();
    } catch (error) {
        
    }

    //parse results
    let result = {
        nickname,
        scenarioType,
        testResult,
    }

    //write results to file
    writeResultsToFile("./results", "results.json", result);
    
    //reset iptables
    enablePing();
}

main();