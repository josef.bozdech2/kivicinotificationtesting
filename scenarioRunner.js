const { writeNotificationSentDialogue, createPromptDialogue } = require("./dialogueMaker");
const { addComment, removeComment, setAcknowledgement, removeAcknowledgement, scheduleDowntime, removeDowntime, disablePing } = require("./testsMaker")

const runScenario = async () => {
    writeNotificationSentDialogue("Comment Added");
    await addComment();
    let commentAdded = createPromptDialogue("Comment Added");

    writeNotificationSentDialogue("Comment Removed");
    await removeComment();
    let commentRemoved = createPromptDialogue("Comment Removed");

    writeNotificationSentDialogue("Acknowledgement Set");
    await setAcknowledgement();
    let acknowledgementSet = createPromptDialogue("Acknowledgement Set");

    writeNotificationSentDialogue("Acknowledgement Removed");
    await removeAcknowledgement();
    let acknowledgementRemoved = createPromptDialogue("Acknowledgement Removed");

    writeNotificationSentDialogue("Downtime Scheduled");
    await scheduleDowntime();
    let downtimeScheduled = createPromptDialogue("Downtime Scheduled");

    writeNotificationSentDialogue("Downtime Removed");
    await removeDowntime();
    let downtimeRemoved = createPromptDialogue("Downtime Removed");

    // writeNotificationSentDialogue("State Change");
    // disablePing();
    // let stateChanged = createPromptDialogue("State Change");
    let notificationResult = {
        commentAdded,
        commentRemoved,
        acknowledgementSet,
        acknowledgementRemoved,
        downtimeScheduled,
        downtimeRemoved,
        // stateChanged,
    }
    return notificationResult;
}

module.exports = {runScenario};