const axios = require("axios").default;
const { exec } = require("child_process");
const data = `root:7592`;
const buff = Buffer.from(data);
const base64data = buff.toString("base64");
axios.defaults.headers.common["Accept"] = "application/json";
axios.defaults.headers.common["Authorization"] = `Basic ${base64data}`;
const _path = "https://147.228.67.28:5665/v1/actions";
const enablePing = () => {
    var command='echo "kivicitester" | sudo -S iptables -D INPUT -p icmp --icmp-type echo-request -j DROP';
    exec(command, (error, stdout, stderr) => {
        
    });
    var command2='echo "kivicitester" | sudo -S iptables -D OUTPUT -p icmp --icmp-type echo-reply -j DROP';
    exec(command2, (error, stdout, stderr) => {
        
    });
}

const disablePing = () => {
    var command='echo "kivicitester" | sudo -S iptables -A INPUT -p icmp --icmp-type echo-request -j DROP';
    exec(command, (error, stdout, stderr) => {
        
    });
    var command2='echo "kivicitester" | sudo -S iptables -A OUTPUT -p icmp --icmp-type echo-reply -j DROP';
    exec(command2, (error, stdout, stderr) => {
        
    });
}

const addComment = async () => {
    try {
        const _data = await axios.post(`${_path}/add-comment`, {
            withCredentials: true,
            headers: {
              Authorization: `Basic ${base64data}`,
              "X-HTTP-Method-Override": "GET",
            },
            "type": "Host",
            "filter": "match(\"testing-server\", host.name)",
            "author": "root",
            "comment": "Notification testing!",
          });
        return _data;
    } catch (error) {
        console.error(error);
    }
    
}

const removeComment = async () => {
    try {
        const _data = await axios.post(`${_path}/remove-comment`, {
            withCredentials: true,
            headers: {
              Authorization: `Basic ${base64data}`,
              Accept: "application/json",
            },
            "type": "Host",
            "filter": "match(\"testing-server\", host.name)",
          });
    //   return _data;
    } catch (error) {
        console.error(error);
    }
}

const setAcknowledgement = async () => {
    try {
        const _data =  await axios.post(`${_path}/acknowledge-problem`, {
            withCredentials: true,
            headers: {
              Authorization: `Basic ${base64data}`,
              Accept: "application/json",
            },
            "type": "Host",
            "filter": "match(\"testing-server\", host.name)",
            "author": "root",
            "comment": "Ackowledgement notification testing!",
          });
      return _data;
    } catch (error) {
        console.error(error);
    }
}

const removeAcknowledgement = async () => {
    try {
        const _data = await axios.post(`${_path}/remove-acknowledgement`, {
            withCredentials: true,
            headers: {
              Authorization: `Basic ${base64data}`,
              Accept: "application/json",
            },
            "type": "Host",
            "filter": "match(\"testing-server\", host.name)",
          });
      return _data;
    } catch (error) {
        console.error(error);
    }
}

const scheduleDowntime = async () => {
    try {
        const _data = await axios.post(`${_path}/schedule-downtime`, {
            withCredentials: true,
            headers: {
              Authorization: `Basic ${base64data}`,
              Accept: "application/json",
            },
            "type": "Host",
            "filter": "match(\"testing-server\", host.name)",
            "author": "root",
            "comment": "Downtime notification testing!",
            "start_time": Date.now(),
            "end_time": Date.now()+100000,
          });
      return _data;
    } catch (error) {
        console.error(error);
    }
}

const removeDowntime = async () => {
    try {
        const _data = await axios.post(`${_path}/remove-downtime`, {
            withCredentials: true,
            headers: {
              Authorization: `Basic ${base64data}`,
              Accept: "application/json",
            },
            "type": "Host",
            "filter": "match(\"testing-server\", host.name)",
          });
      return _data;
    } catch (error) {
        console.error(error);
    }
}



module.exports = { disablePing, enablePing, addComment, removeComment, setAcknowledgement, removeAcknowledgement, scheduleDowntime, removeDowntime}