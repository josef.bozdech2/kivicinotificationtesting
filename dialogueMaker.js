const prompt = require('prompt-sync')();

const writeNotificationSentDialogue = (notification) => {
    console.log(`Creating ${notification} action...`);
}

const createPromptDialogue = (notification) => {
    // await sleep(100);
    return prompt(`Did you receive ${notification} type notification? `);
}

const printScenarioOptions = () => {
    console.log("1)Notifications on with app on");
    console.log("2)Notifications on with app closed");
    console.log("3)Notification types off");
    console.log("4)Notification interval outside of current time");
}

const printScenarioDialogue = (scenarioType) => {
    const _login = "username: root\npassword: 7592\nIP: 104.248.131.64\nport: 5665\n"
    const _notificationTypes = "State Change\nComment Added\nComment Removed\nAcknowledgement Set\nAcknowledgement Removed\nDowntime Added\nDowntime Removed\nDowntime Started\nDowntime Triggered\n";
    if(scenarioType == "1"){
        prompt("Please start your application, then hit enter.");
        prompt("Via menu, go to Settings screen, then hit ok.");
        console.log(`Please fill in the login credentials:\n${_login}`);
        prompt(`Then turn on following notification types, then press enter:\n${_notificationTypes}`);
        prompt("Please set Notification interval to 00:00 - 23:59, then hit enter.");
        prompt("Press Save button in the upper right corner, then hit enter.");
        prompt("Please, wait one minute before continuing in the test. Schedulers needs to schedule your connection.")
        prompt("After hitting enter, you will be asked series of questions regarding notifications comming to your device. Please answer with Yes (y) or No (n). Please keep in mind that notifications are sent every 15 seconds. Press enter to proceed.");
        return;
    }
    if(scenarioType == "2"){
        prompt("Please start your application, then hit enter.");
        prompt("Via menu, go to Settings screen, then hit ok.");
        console.log(`Please fill in the login credentials:\n${_login}`);
        prompt(`Then turn on following notification types, then press enter:\n${_notificationTypes}`);
        prompt("Press Save button in the upper right corner, then hit enter.");
        prompt("Please set Notification interval to 00:00 - 23:59, then hit enter.");
        prompt("Turn off your KivIci application, then press ok.");
        prompt("Please, wait one minute before continuing in the test. Schedulers needs to schedule your connection.")
        prompt("After hitting enter, you will be asked series of questions regarding notifications comming to your device. Please answer with Yes (y) or No (n). Please keep in mind that notifications are sent every 15 seconds. Press enter to proceed.");
        return;
    }
    if(scenarioType == "3"){
        prompt("Please start your application, then hit enter.");
        prompt("Via menu, go to Settings screen, then hit ok.");
        console.log(`Please fill in the login credentials:\n${_login}`);
        prompt(`Then turn off following notification types, then press enter:\n${_notificationTypes}`);
        prompt("Please set Notification interval to 00:00 - 23:59, then hit enter.");
        prompt("Press Save button in the upper right corner, then hit enter.");
        prompt("Please, wait one minute before continuing in the test. Schedulers needs to schedule your connection.")
        prompt("After hitting enter, you will be asked series of questions regarding notifications comming to your device. Please answer with Yes (y) or No (n). Please keep in mind that notifications are sent every 15 seconds. Press enter to proceed.");
        return;
    }
    if(scenarioType == "4"){
        prompt("Please start your application, then hit enter.");
        prompt("Via menu, go to Settings screen, then hit ok.");
        console.log(`Please fill in the login credentials:\n${_login}`);
        prompt(`Then turn on following notification types, then press enter:\n${_notificationTypes}`);
        prompt("Please set Notification interval to 00:00 - 00-01, then hit enter.");
        prompt("Press Save button in the upper right corner, then hit enter.");
        prompt("Please, wait one minute before continuing in the test. Schedulers needs to schedule your connection.")
        prompt("After hitting enter, you will be asked series of questions regarding notifications comming to your device. Please answer with Yes (y) or No (n). Please keep in mind that notifications are sent every 15 seconds. Press enter to proceed.");
        return;
    }
}
  
  function sleep(ms) {
    return new Promise((resolve) => {
      setTimeout(resolve, ms);
    });
  }

module.exports = {writeNotificationSentDialogue, createPromptDialogue, printScenarioOptions, printScenarioDialogue};